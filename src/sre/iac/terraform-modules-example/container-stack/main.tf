locals {
  prefix = "terraform-module-example"
}

resource "random_pet" "pet" {
  keepers = {
    image_name = var.image_name
  }
  length = 1
}

resource "docker_network" "my_network" {
  name = "${local.prefix}-${random_pet.pet.id}"
  driver = "bridge"
}

# resource <resource_type> <resource_name(ID)>
resource "docker_image" "nginx" {
  name         = var.image_name
  keep_locally = false
}

# dependencies
resource "docker_container" "nginx" {

  count = var.container_count
  image = docker_image.nginx.name
  name  = "${local.prefix}-${count.index}"

  # ports {
  #   internal = 80
  #   external = "808${count.index}"
  # }

  labels {
    label = "environment"
    value = "development"
  }

  labels {
    label = "image-txt"
    value = "The image is ${docker_image.nginx.name}"
  }

  networks_advanced {
    name = docker_network.my_network.name
  }
}


