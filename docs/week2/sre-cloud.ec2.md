# Cloud


## AWS EC2

Virutal Machines (VMs) are a set of computer hardware and software that are designed to run on a single computer.

### Pre requisites

- [x] have a basic understanding of linux OS.
- [x] have a basic understanding of bash scripting.
- [x] use package managers or have a basic understanding of how to install packages.


### Hello World on EC2



EC2 lets you create and run virtual machines on AWS infrastructure. EC2 provides control of every component but also assumes it’s your responsibility to keep things running. With VMs, you obviously need to go ahead and install/configure your own web hosting stack from scratch (and also decide how to provide for load balancing and scalability, from a variety of options.)

The AWS documentation looks extremely useful, but who starts by reading the manual? AWS Search to the rescue:

- “AWS EC2 create a vm”
- “AWS EC2 ssh into vm”
- “install python linux”
- “hello world python”
- “hello world python AWS EC2”…


### Plan

- Create a VM on EC2 (Linux obviously)
- Install NodeJS in the VM.
- Setup Pokemon app provided in the repo.
- Run the app and expose it on a public IP address.
- Can we scale?



![App EC2](static/img/poke-ec2.png)

## 1. Create a VM on EC2

Navigate to VM Instances page in AWS Console and go through the setup process.

- AMI: Ubuntu Server 20.04 LTS (HVM), SSD Volume Type
- Instance type: t2.medium (1 CPU, 1GB RAM)
- Instance name: changed to helloworld-vm
- Auto-assign Public IP : Enable
- VPC: default
- Storage: 8GiB SSD
- Configure Security Group: TCP 5000 MyIP
- Create a new key pair RSA


Hit the Create button. The VM instance is up and running in literally a few seconds.

- Connect via SSH or Console


### Common packages

```bash
sudo apt-get update
sudo apt-get install -y curl git


curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

sudo usermod -aG docker $USER


sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# to reload group
exit
```

## 2. Install Redis 


## 3. Install  Python on the VM





## 5. Run the app and expose it on a public IP address.

I search on [“AWS EC2 open firewall ports”], which brings me to [this link](https://intellipaat.com/community/3700/how-to-open-a-web-server-port-on-ec2-instance). Then I open port 5000 for both http and https using Console (Networking > VPC Network):


??? note
    change https:// to http://


## 6. Can we scale?

EC2 does offer [load balancing and autoscaling for groups of instances](https://docs.aws.amazon.com/autoscaling/ec2/userguide/AutoScalingGroup.html) but it requires additional setup, maintenance, as well as general understanding of scalable web architectures and distributed systems design

![ASG](static/img/asg.png)


## Things to consider:

- Launch templates
- Startup-scripts
- [ASG](https://docs.aws.amazon.com/autoscaling/ec2/userguide/AutoScalingGroup.html)
- [Tutorial](https://docs.aws.amazon.com/autoscaling/ec2/userguide/create-asg-launch-template.html)
